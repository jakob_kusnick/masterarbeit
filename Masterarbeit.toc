\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Technische Grundlagen}{3}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Computertomografie}{3}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.1}Technik}{3}{subsubsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.2}Artefakte bei der Computertomografie}{5}{subsubsection.2.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Volume Rendering}{8}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Virtual Reality}{10}{subsection.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Augmented Reality}{11}{subsection.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Verwandte Arbeiten}{13}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Visualisierung von CT-Daten}{13}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Computergrafik im Museumsumfeld}{16}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Digitale Musikinstrumente}{19}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Projekt MUSICES}{19}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Die Instrumente}{20}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1}Tastenzister}{21}{subsubsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2}Drehleier}{21}{subsubsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.3}B\IeC {\"u}chsentrompete}{21}{subsubsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.4}Damaru}{22}{subsubsection.4.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.5}Handharmonika}{22}{subsubsection.4.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.6}Zither}{23}{subsubsection.4.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Datenverarbeitung und -analyse}{24}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Datenverarbeitung}{24}{subsection.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Datenanalyse}{27}{subsection.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.1}Histogramme}{27}{subsubsection.5.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.2}Bildanalyse}{29}{subsubsection.5.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.3}Konturbaumanalyse}{35}{subsubsection.5.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Framework - \IeC {\"U}bersicht}{38}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Anforderungen an eine digitale Museumsausstellung}{39}{subsection.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Renderer}{39}{subsection.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}Szene}{40}{subsection.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.4}Skript}{40}{subsection.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Framework - Implementierungsdetails}{41}{section.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1}Rendering}{41}{subsection.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2}Volume Rendering}{41}{subsection.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.1}Implementierung}{41}{subsubsection.7.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.2}Interaktionsm\IeC {\"o}glichkeiten}{43}{subsubsection.7.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.3}Definition der Szenen mittels Lua-Skript}{45}{subsection.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.4}Objekte}{46}{subsection.7.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.4.1}Punkte von Interesse}{46}{subsubsection.7.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.4.2}Annotationen}{47}{subsubsection.7.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.4.3}Regionen von Interesse}{47}{subsubsection.7.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.5}Geschwindigkeit}{48}{subsection.7.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Anwendungsbeispiel}{50}{section.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1}Bedienung am Computer}{50}{subsection.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.1.1}Beispielszene}{50}{subsubsection.8.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.2}Darstellung in der erweiterten Realit\IeC {\"a}t}{53}{subsection.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.2.1}Beispielszene}{54}{subsubsection.8.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Offene Probleme und zuk\IeC {\"u}nftige Arbeit}{57}{section.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.1}Datenanalyse}{57}{subsection.9.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.2}Datenverarbeitung}{57}{subsection.9.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.3}Darstellung}{58}{subsection.9.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {10}Zusammenfassung}{60}{section.10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {11}Danksagung}{61}{section.11}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {12}Literaturverzeichnis}{I}{section.12}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {13}Abbildungsverzeichnis}{III}{section.13}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {14}Erkl\IeC {\"a}rung}{VI}{section.14}
